import {ConfigForm, Macro, render, Text, TextArea, useConfig} from "@forge/ui";

const App = () => {
    const config = useConfig();
    const text = config.text;
    const arr = text.split("\n");
    const randomQuote = arr[Math.floor(Math.random() * arr.length)];

    return <Text content={randomQuote} />;
};

const Config = () => {
    return (
        <ConfigForm>
            <TextArea name="text" label="Quote set" />
        </ConfigForm>
    );
};

export const run = render(
    <Macro
        app={<App/>}
        config={<Config />}
        defaultConfig={{
            text: "Insert some quotes here.\nEach quote - each line."
        }}
    />
);
